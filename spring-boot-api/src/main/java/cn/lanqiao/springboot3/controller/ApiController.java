package cn.lanqiao.springboot3.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.tags.Tag;

import cn.lanqiao.springboot3.common.Result;
import cn.lanqiao.springboot3.common.ResultGenerator;
import cn.lanqiao.springboot3.dao.UserDao;
import cn.lanqiao.springboot3.entity.User;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

// import java.util.List;
import java.util.*;

/**
 * @author 杨驰
 * @email 18210427950@qq.com
 */
@Tag(name = "用户模块接口")
@Controller
@RequestMapping("/api")
public class ApiController {

    @Resource
    UserDao userDao;

    // 查询一条记录
    @Operation(summary = "获取用户详细信息", description = "根据id来获取用户详细信息")
    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Result<User> getOne(@PathVariable("id") Integer id) {
        if (id == null || id < 1) {
            return ResultGenerator.genFailResult("缺少参数");
        }
        User user = userDao.getUserById(id);
        if (user == null) {
            return ResultGenerator.genFailResult("无此数据");
        }
        return ResultGenerator.genSuccessResult(user);
    }

    // 查询所有记录
    @Operation(summary = "获取用户列表")
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    @ResponseBody
    public Result<List<User>> queryAll() {
        List<User> users = userDao.findAllUsers();
        return ResultGenerator.genSuccessResult(users);
    }

    //  // 查询所有记录根据ID正序查询
    // @Operation(summary = "获取用户列表正序")
    // @RequestMapping(value = "/users/lists", method = RequestMethod.GET)
    // @ResponseBody
    // public Result<List<User>> queryAll() {
    //     List<User> users = userDao.findAllUsersByIdAsc();
    //     return ResultGenerator.genSuccessResult(users);
    // }

    // 新增一条记录
    @Operation(summary = "新增用户", description = "根据User对象新增用户")
    @RequestMapping(value = "/users", method = RequestMethod.POST)
    @ResponseBody
    public Result<Boolean> insert(@RequestBody User user) {
        // 参数验证
        if (StringUtils.isEmpty(user.getName()) || StringUtils.isEmpty(user.getPassword())) {
            return ResultGenerator.genFailResult("缺少参数");
        }
        return ResultGenerator.genSuccessResult(userDao.insertUser(user) > 0);
    }

    // 修改一条记录
    @Operation(summary = "更新用户详细信息", description = "")
    @RequestMapping(value = "/users", method = RequestMethod.PUT)
    @ResponseBody
    public Result<Boolean> update(@RequestBody User tempUser) {
        //参数验证
        if (tempUser.getId() == null || tempUser.getId() < 1 || StringUtils.isEmpty(tempUser.getName()) || StringUtils.isEmpty(tempUser.getPassword())) {
            return ResultGenerator.genFailResult("缺少参数");
        }
        //实体验证，不存在则不继续修改操作
        User user = userDao.getUserById(tempUser.getId());
        if (user == null) {
            return ResultGenerator.genFailResult("参数异常");
        }
        user.setName(tempUser.getName());
        user.setPassword(tempUser.getPassword());
        return ResultGenerator.genSuccessResult(userDao.updUser(user) > 0);
    }

    // 删除一条记录
    @Operation(summary = "删除用户", description = "根据id删除对象")
    @RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Result<Boolean> delete(@PathVariable("id") Integer id) {
        if (id == null || id < 1) {
            return ResultGenerator.genFailResult("缺少参数");
        }
        return ResultGenerator.genSuccessResult(userDao.delUser(id) > 0);
    }

}
